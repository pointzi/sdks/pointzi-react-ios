//
//  Pointzi_react_ios.h
//  Pointzi-react-ios
//
//  Created by ganesh faterpekar on 4/30/20.
//  Copyright © 2020 Streethawk. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for Pointzi_react_ios.
FOUNDATION_EXPORT double Pointzi_react_iosVersionNumber;

//! Project version string for Pointzi_react_ios.
FOUNDATION_EXPORT const unsigned char Pointzi_react_iosVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Pointzi_react_io/PublicHeader.h>

#include <Pointzi_react_ios/PointziReact.h>
