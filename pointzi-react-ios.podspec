Pod::Spec.new do |spec|
  spec.name         = "pointzi-react-ios"
  spec.version      = "0.0.1"
  spec.summary      = "pointzi-react-ios module"
  spec.description  = <<-DESC
                    This is the React Native wrapper for Pointzi that allows the use of Pointzi in React Native Environment.
                   DESC
  spec.homepage     = "https://pointzi.com"
  spec.license      = { :type => 'Permissive Binary License', :file => "LICENSE" }
  spec.author       = { 'StreetHawk' => 'support@streethawk.com' }
  spec.platform     = :ios, "8.0"
  spec.documentation_url   = "https://dashboard.pointzi.com/docs/sdks/ios/integration/"
  spec.source       = { :git => "https://gitlab.com/pointzi/sdks/pointzi-react-ios.git"} #:tag => "#{spec.version}" }
  spec.source_files = 'pointzi-react-ios/*.{m,h}'
  spec.dependency "React"
end

